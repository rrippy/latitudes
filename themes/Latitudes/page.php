<?php get_header(); ?>

    <div id="main" class="clearfix">
        
        <?php
			// $sub_categories = get_field('sub-categories');
			// $intro_text = get_field('large_intro_text');
			// $background = get_field('background_image');
		?>
 		
        <section class="section_<?php the_ID(); ?>">
			<?php if ($background) { ?>
                <div class="background_section"><img src="<?= $background ?>"</div>
            <?php } ?>
    	
        <div class="section_content">
        
    	<header class="main_title">
        
            <h2><?php the_title(); ?></h2>
        
        </header>



         <?php if ($intro_text) { ?>
              <div class="intro_text"><?= $intro_text ?></div>
        <?php } ?>
        
        
        
        <?php if(get_the_content()) { ?>
			<div class="section_main_content">
				<?php the_content();?>
			</div>      
        <?php } ?>
 
        

        <?php if($sub_categories): ?>
        
        	<div class="category_blocks"> 
          
			<?php foreach($sub_categories as $sub_category): ?>
              
              <div class="category <?= $sub_category['block_slug'] ?>">
                 
                <?php if($sub_category['category_title']): ?>
                    <h3><?= $sub_category['category_title'] ?></h3>
                    <div class="seperator"></div>
                <?php endif; ?>
                
                <?php if($sub_category['category_text']): ?>
                    <?= $sub_category['category_text'] ?>
                <?php endif; ?>
              
              </div><!--.category-->  
                    
            <?php endforeach; ?>
            
            <div class="clear"></div>
            
            </div><!--.category_block--> 


        <?php endif;?>
        
        </div><!--.section_content-->
        
        
        
    </section>
  
        
        
    </div><!-- #main -->
   


<?php get_footer(); ?>