<?php
/**
 * The template for displaying all posts.
 *
 * Default Post Template
 *
 * Page template with a fixed 940px container and right sidebar layout
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header(); ?>

	<div id="main" class="clearfix">
    
    
    <div class="section_wrapper">
        
        
   
   <div class="section_main_content">
		
		<div id="primary">
		<?php while ( have_posts() ) : the_post(); ?>
  
        	 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                
                <div class="job_content">
                
                	<header>
                         <h2><?php the_title();?></h2>
                         <?php the_time('F j, Y'); ?>
                         <div class="author_name">by <?php the_author(); ?></div>
                    </header>
             
                	<?php the_content();?>
                    
                    <div class="more_blog"><a href="<?php echo esc_url( home_url( '/' ) ); ?>blog/" >Back to blog</a></div>
                    
                
                </div><!--.job_content-->
              
           </article>
        
        <?php endwhile; // End the loop ?>
        
        </div><!-- #primary -->

        
        
    </div><!-- .section_main_content -->
    
    
    </div><!--.section_wrapper-->
    
    </div><!-- #main -->



<?php get_footer(); ?>