<?php get_header(); the_post(); 

$recent_posts = get_posts(array(
	'post_type' => 'post',
	'numberposts' => 20,
	'post_status' => 'publish',
	'order' => 'DESC',
	'orderby' => 'post_date'
));

?>

    <div id="main" class="clearfix">
    
    
    <div class="section_wrapper">
    
    
    	<header class="main_title">
        
            <h2>Blog</h2>
        
        </header>
    
    
    <div class="section_main_content clearfix">
    
    
     <?php // news section
			if($recent_posts): ?>
                    
					<?php foreach($recent_posts as $post): setup_postdata($post); ?>
                        <article>
                            <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>"><h3><?php the_title(); ?></h3></a>
                            <?php the_time('F j, Y'); ?>
                            <div class="author_name"><?php the_author(); ?></div>
                            <?php the_content(); ?>
                       </article>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
            
            <?php endif; ?>
     
    
        
        
        	
        </div><!-- .section_main_content -->
        
        </div><!-- .section_wrapper -->
    </div><!-- #main -->    



<?php get_footer(); ?>