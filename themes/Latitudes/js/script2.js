$(window).load(function() {
	
	var doc_height = $(window).height();
	var doc_width = ($(window).width());	
	
	
	
	// home image height and width 
	$('.home_slider.flexslider .flex-active-slide img').each(function() {
		var horOffset = ($(this).width() - doc_width);
		var verOffset = ($(this).height() - doc_height);
		if (verOffset < horOffset) {
			$(this).css('height', '100%');
			$(this).css('width', 'auto');
		}
	});

	

	if ((doc_width / doc_height) > 1.4) {
		// if it's not a vertical window
		var parallax_height = Math.round($('.parallax_img img').height() * 0.76);
		$('.parallax_img').css('height', parallax_height);
	}
	
});






// parallax nav
$(window).scroll(function(){
  var win_height = ($(window).height());
  var scrollTop = $(window).scrollTop();
  var win_width = ($(window).width());
  if (win_width > 499) {
	  if (scrollTop < (win_height - 71)) {
		  $('body.home #header_wrap').slideUp();  
	  } else {
		 $('body.home #header_wrap').slideDown();
	  }
  }
});







$(document).ready(function(){
	
	// set up gallery sliders
	function sortGallery(){
		var guidesItems = $('ul.gallery_items');
		
		var guidesItems = myList.children('li.guides').get();
		var tripsItems = myList.children('li.trips').get();
		var speciesItems = myList.children('li.species').get();
		
		$('.guides_slider ul').append(guidesItems);
		$('.trips_slider ul').append(tripsItems);
		$('.species_slider ul').append(speciesItems);
		
		
	};



	
	
	
	// flex slider - home page
	$('.home_slider.flexslider').flexslider({
		animation: "fade",
		controlNav: false,
		directionNav: false,
		slideshow: true
	});
	
	// flex slider - gallery
	$('.flexslider.Guides_slider, .flexslider.Trips_slider, .flexslider.Species_slider').flexslider({
		animation: "fade",
		controlNav: false,
		directionNav: true,
		slideshow: false
	});
	
	
	// slider height
	var win_height = ($(window).height());
	var win_width = ($(window).width());
	// top nav bug for iphones
	if (navigator.userAgent.match(/iPhone/i)) {
		win_height = (win_height + 60);
	}
	$('.home_slider.flexslider').css('height', win_height);
	$('.home_slider .slides li').css('height', win_height);
	
	
	

  
  
  
  
  
  
  // parallax image height
  	if ((win_width / win_height) < 1.35) {
		// if it's a vertical window
		$('.parallax_img img').css('height', '174%');
		$('.parallax_img img').css('width', 'auto');
		
		// parallax images	
		$.stellar({
		  // Set scrolling to be in either one or both directions
		  horizontalScrolling: false,
		  verticalScrolling: true,
		  verticalOffset: 600,
		  horizontalOffset: 0,
		  responsive: true,
		  scrollProperty: 'scroll',
		  positionProperty: 'position',
		  parallaxBackgrounds: false,
		  parallaxElements: true,
	  });
	  
	} else {
		// parallax images	
	$.stellar({
		  // Set scrolling to be in either one or both directions
		  horizontalScrolling: false,
		  verticalScrolling: true,
		  verticalOffset: 186,
		  horizontalOffset: 0,
		  responsive: true,
		  scrollProperty: 'scroll',
		  positionProperty: 'position',
		  parallaxBackgrounds: false,
		  parallaxElements: true,
	  });
		
	}
  
  

  
  
  
  
  // gallery categories
  $('.Guides_btn a, .Trips_btn a, .Species_btn a').click(function(e) {
		e.preventDefault();
		$('.gallery_btn a').removeClass('selected');
		$(this).addClass('selected');
		gallery_show();
  });
  
  // set gallery height
  var galWidth = $('.flexslider.gallery').width();
  var galHeight = (galWidth * 0.66932725);
  $('.gallery_all').css('height', galHeight);
  
  
  
  
  $('.Guides_btn a').addClass('selected');
  gallery_show();
  
  function gallery_show() {
	  $('.flexslider.gallery').fadeOut();
	  if ($('.Guides_btn a').hasClass('selected')) {
		 $('.Guides_slider').fadeIn();
	  } else if ($('.Trips_btn a').hasClass('selected')) {
		 $('.Trips_slider').fadeIn();
	  } else if ($('.Species_btn a').hasClass('selected')) {
		 $('.Species_slider').fadeIn();
	  }
  }
  
  
  
	
  // main nav
  
  var win_width = $(window).width();
  
  
  
  // main navigation scroll
  
  $('.paralax_nav a').addClass('isTop');   
  
  $('#branding #site_title a').click(function(e) {
	  if ($('body').hasClass('single') || $('body').hasClass('page-id-17')) {
		} 
		else {
			e.preventDefault();
			// scroll document
			$.scrollTo(0,'slow');
		}
   });
   
	$('.what_btn > a').click(function(e) {
		if ($('body').hasClass('single') || $('body').hasClass('page-id-17')) {
		} else if (win_width < 500) {
			e.preventDefault();
			$.scrollTo('.section_7','slow', {
				offset:30
			 });	
		} else if (win_width > 1000) {
			e.preventDefault();
			$.scrollTo('.section_7','slow', {
				offset:-71
			 });
		} else {
			e.preventDefault();
			$.scrollTo('.section_7','slow', {
				offset:-110
			 });
		}
   });
   
   $('.how_btn > a').click(function(e) {
		if ($('body').hasClass('single') || $('body').hasClass('page-id-17')) {
		} else if (win_width < 500) {
			e.preventDefault();
			$.scrollTo('.section_8','slow', {
				offset:30
			 });	
		} else if (win_width > 1000) {
			e.preventDefault();
			$.scrollTo('.section_8','slow', {
				offset:-70
			 });
		} else {
			e.preventDefault();
			$.scrollTo('.section_8','slow', {
				offset:-110
			 });
		}
   });
   
   $('.gallery_bn > a').click(function(e) {
	   if ($('body').hasClass('single') || $('body').hasClass('page-id-17')) {
		} else if (win_width < 500) {
			e.preventDefault();
			$.scrollTo('.section_9','slow', {
				offset:30
			 });	
		} else if (win_width > 1000) {
			e.preventDefault();
			$.scrollTo('.section_9','slow', {
				offset:-70
			 });
		} else {
			e.preventDefault();
			$.scrollTo('.section_9','slow', {
				offset:-110
			 });
		}
   });
   
   $('.about_btn > a').click(function(e) {
	   if ($('body').hasClass('single') || $('body').hasClass('page-id-17')) {
		} else if (win_width < 500) {
			e.preventDefault();
			$.scrollTo('.section_10','slow', {
				offset:30
			 });	
		} else if (win_width > 1000) {
			e.preventDefault();
			$.scrollTo('.section_10','slow', {
				offset:-70
			 });
		} else {
			e.preventDefault();
			$.scrollTo('.section_10','slow', {
				offset:-110
			 });
		}
   });
   
   $('.blog_btn > a').click(function(e) {
	   if ($('body').hasClass('single') || $('body').hasClass('page-id-17')) {
		} else if (win_width < 500) {
			e.preventDefault();
			$.scrollTo('.section_17','slow', {
				offset:30
			 });	
		} else if (win_width > 1000) {
			e.preventDefault();
			$.scrollTo('.section_17','slow', {
				offset:-70
			 });
		} else {
			e.preventDefault();
			$.scrollTo('.section_17','slow', {
				offset:-110
			 });
		}
   });
   
   $('.contact_btn > a').click(function(e) {
	   if ($('body').hasClass('single') || $('body').hasClass('page-id-17')) {
		} else if (win_width < 500) {
			e.preventDefault();
			$.scrollTo('.section_11','slow', {
				offset:30
			 });	
		} else if (win_width > 1000) {
			e.preventDefault();
			$.scrollTo('.section_11','slow', {
				offset:-70
			 });
		} else {
			e.preventDefault();
			$.scrollTo('.section_11','slow', {
				offset:-110
			 });
		}
   });
   
   
   
   
   // header nav for blog
   $('body.single .what_btn a, body.page-id-17 .what_btn a').attr('href', 'http://latitudesoutfitting.com/#7_bookmark');

    $('body.single .how_btn a, body.page-id-17 .how_btn a').attr('href', 'http://latitudesoutfitting.com/#8_bookmark');
	
    $('body.single .gallery_bn a, body.page-id-17 .gallery_bn a').attr('href', 'http://latitudesoutfitting.com/#9_bookmark');

    $('body.single .about_btn a, body.page-id-17 .about_btn a').attr('href', 'http://latitudesoutfitting.com/#10_bookmark');
	
    $('body.single .contact_btn a, body.page-id-17 .contact_btn a').attr('href', 'http://latitudesoutfitting.com/#11_bookmark');
   
   
   
  
   
   
   
   
   // bug orientation portrait/lanscape IOS //
	if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
	var viewportmeta = document.querySelector('meta[name="viewport"]');
	if (viewportmeta) {
		viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
		document.addEventListener('orientationchange', function () {
			viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1';
		}, false);
	  }
	}
	
	
	
	
	
  $('.home_slider.flexslider li').addEventListener('DOMAttrModified', function(e){
	  alert ("yes");
	  if (e.attrName === 'display') {
		console.log('prevValue: ' + e.prevValue, 'newValue: ' + e.newValue);
	  }
	}, false);

  
});





// home image height
function homeSlides() {
   
   $('.home_slider.flexslider .background_section img').each(function() {
		var horOffset = ($(this).width() - doc_width);
		var verOffset = ($(this).height() - doc_height);
		if (verOffset < horOffset) {
			$(this).css('height', '100%');
			$(this).css('width', 'auto');
		}
	});

}






// when browser is resized
$(window).resize(function() {
	
	// slider height
	var win_height = ($(window).height());
	var win_width = ($(window).width());
	// top nav bug for iphones
	if (navigator.userAgent.match(/iPhone/i)) {
		win_height = (win_height + 60);
	}
	$('.home_slider.flexslider').css('height', win_height);
	$('.home_slider .slides li').css('height', win_height);
	
	var doc_height = $(window).height();
	var doc_width = ($(window).width());	
	
	
	
	// home image height and width 
	$('.home_slider.flexslider .background_section img').each(function() {
		var horOffset = ($(this).width() - doc_width);
		var verOffset = ($(this).height() - doc_height);
		if (verOffset < horOffset) {
			$(this).css('height', '100%');
			$(this).css('width', 'auto');
		}
	});

	

	if ((doc_width / doc_height) > 1.4) {
		// if it's not a vertical window
		var parallax_height = Math.round($('.parallax_img img').height() * 0.76);
		$('.parallax_img').css('height', parallax_height);
	}
	
	// parallax image height
  	if ((win_width / win_height) < 1.35) {
		// if it's a vertical window
		$('.parallax_img img').css('height', '174%');
		$('.parallax_img img').css('width', 'auto');
		
		// parallax images	
		$.stellar({
		  // Set scrolling to be in either one or both directions
		  horizontalScrolling: false,
		  verticalScrolling: true,
		  verticalOffset: 600,
		  horizontalOffset: 0,
		  responsive: true,
		  scrollProperty: 'scroll',
		  positionProperty: 'position',
		  parallaxBackgrounds: false,
		  parallaxElements: true,
	  });
	  
	} else {
		// parallax images	
	$.stellar({
		  // Set scrolling to be in either one or both directions
		  horizontalScrolling: false,
		  verticalScrolling: true,
		  verticalOffset: 186,
		  horizontalOffset: 0,
		  responsive: true,
		  scrollProperty: 'scroll',
		  positionProperty: 'position',
		  parallaxBackgrounds: false,
		  parallaxElements: true,
	  });
		
	}
	// set gallery height
	  var galWidth = $('.flexslider.gallery').width();
	  var galHeight = (galWidth * 0.66932725);
	  $('.gallery_all').css('height', galHeight);
	
});