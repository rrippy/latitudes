<?php get_header(); ?>

    <div id="main" class="clearfix">
    
    <!-- select home pages -->
    <!-- page order -->
   <?php $home_pages = array(7,8,9,10,17,11);
	foreach( $home_pages as $a_page ) {
    	$special_tabs = new WP_Query('page_id='.$a_page);

    	if ($special_tabs->have_posts()) : while ($special_tabs->have_posts()) : $special_tabs->the_post(); ?>
        
        <?php
			// $sub_categories = get_field('sub-categories');
			// $intro_text = get_field('large_intro_text');
			// $background = get_field('background_image');
		
        
        $recent_jobs = get_posts(array(
			'post_type' => 'post',
			'numberposts' => 5,
			'post_status' => 'publish',
			'order' => 'DESC',
			'orderby' => 'post_date',
			'category' => 'jobs'
		));
		
		?>

 		
        <section class="section_<?php the_ID(); ?>">
        	
            <a name="<?php the_ID(); ?>_bookmark" class="bookmark"></a>
            
			<?php if ($background) { ?>
                <div class="background_section"><img src="<?= $background ?>"</div>
            <?php } ?>
    	
        <div class="section_content">
        
    	<header class="main_title">
        
            <h2><?php the_title(); ?></h2>
        
        </header>



         <?php if ($intro_text) { ?>
              <div class="intro_text"><?= $intro_text ?></div>
        <?php } ?>
        
        
        
        <?php if(get_the_content()) { ?>
			<div class="section_main_content">
				<?php the_content();?>
			</div>      
        <?php } ?>
 
        

        <?php if($sub_categories): ?>
        
        	<div class="category_blocks"> 
          
			<?php foreach($sub_categories as $sub_category): ?>
              
              <div class="category <?= $sub_category['block_slug'] ?>">
                 
                <?php if($sub_category['category_title']): ?>
                    <h3><?= $sub_category['category_title'] ?></h3>
                    <div class="seperator"></div>
                <?php endif; ?>
                
                <?php if($sub_category['category_text']): ?>
                    <?= $sub_category['category_text'] ?>
                <?php endif; ?> 
                
              
              </div><!--.category-->  
                    
            <?php endforeach; ?>
            
            <div class="clear"></div>
            
            </div><!--.category_block--> 

        <?php endif;?>
        
        
        <!-- jobs page -->
		<?php
         if ( get_the_id() == 47 ): ?>
             <?php foreach($recent_jobs as $post): setup_postdata($post); ?>
                <ul>
                    <li class="clearfix">
                        <h3><a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>"><?php the_title(); ?></a></h3>
                    </li>
                 </ul>
              <?php endforeach; ?>
              <div class="all_jobs"><a href="<?php echo esc_url( home_url( '/' ) ); ?>opportunities/">Read more</a></div>
        
        <?php endif; ?> 
        
        
        	<!-- contact social info -->
        	<?php
        	 if ( get_the_id() == 13 ): ?>
      			<a href="http://www.linkedin.com/company/3007258?goback=%2Efcs_GLHD_Paradox+resources_false_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2&trk=NUS_CMPY_TWIT" 
                target="_blank" class="linkedin social_icon"></a>
                <a href="<?php bloginfo('template_url'); ?>/images/paradox_brochure.pdf" 
                target="_blank" class="download social_icon">Brochure</a>
      		<?php endif; ?> 
            
        
        </div><!--.section_content-->
        
        
        
    </section>

    <?php endwhile; endif; 
    	/* loop ends */
	} ?>   
        
        
    </div><!-- #main -->
   


<?php get_footer(); ?>