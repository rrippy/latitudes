<?php
/**
 * Template Name: Jobs Blog
 * Description: Page template to display job posts
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header(); ?>

<div id="main" class="clearfix">

	<div class="jobs_image"><img src="<?php bloginfo('template_url'); ?>/images/ParadoxJobs.jpg" /></div>

	<div class="section_content">

<?php while ( have_posts() ) : the_post(); ?>
  <div class="container">
 <!-- Masthead
 ================================================== -->
 <header class="jumbotron subhead" id="overview">
  <h2>Paradox Opportunities</h2>
</header>

<div class="row content">
  <div class="span8">
    <?php the_content();
    endwhile;
           // end of the loop
    wp_reset_query();
          // resetting the loop
    ?>
    <hr />
  </div><!-- /.span8 -->

  <div class="span8">
    <?php
              // Blog post query
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    query_posts( array( 'post_type' => 'post', 'paged'=>$paged, 'showposts'=>0) );
    if (have_posts()) : while ( have_posts() ) : the_post(); ?>
    <div <?php post_class(); ?>>
      <a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><h3><?php the_title();?></h3></a>
      <div class="row">
        <div class="span2"><?php // Checking for a post thumbnail
        if ( has_post_thumbnail() ) ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
          <?php the_post_thumbnail();?></a>
        </div><!-- /.span2 -->
        
        <?php $job_excerpt = get_field('job_excerpt'); ?>
        <?php if ($job_excerpt) { ?>
              <div><?= $job_excerpt ?></div>
              <div class="readmore"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >Read more ></a></div>
        <?php } ?>
        
        <?php // the_excerpt();?>
     </div><!-- /.row -->
     <hr />
   </div><!-- /.post_class -->
 <?php endwhile; endif; ?>
 
 </div><!--.section_content-->

</div><!-- #main -->

<?php get_footer(); ?>