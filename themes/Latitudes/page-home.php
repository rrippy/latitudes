<?php get_header(); ?>

    <div id="main" class="clearfix">
    
    <!-- select home pages -->
    <!-- page order -->
   <?php $home_pages = array(6,7,60,8,9,62,10,17,11);
	foreach( $home_pages as $a_page ) {
    	$special_tabs = new WP_Query('page_id='.$a_page);

    	if ($special_tabs->have_posts()) : while ($special_tabs->have_posts()) : $special_tabs->the_post(); ?>
        	
        <?php
			$sub_categories = get_field('sub-categories');
			$intro_text = get_field('intro_text');
			$backgrounds = get_field('background_images');
			$parallax_image = get_field('parallax_image');
			$gallery_categories = get_field('gallery_categories');
			
			
			$recent_posts = get_posts(array(
				'post_type' => 'post',
				'numberposts' => 3,
				'post_status' => 'publish',
				'order' => 'DESC',
				'orderby' => 'post_date'
			));

		?>
        
        
     
        
        
<!-- home page -->
<?php if ( get_the_id() == 6 ): ?> 

	 <div class="paralax_nav">
    	<div class="large_logo"></div>
        <?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
    </div>
       
	<?php if($backgrounds): ?>
            
    <div class="flexslider home_slider">
 
      <ul class="slides">
        <?php foreach($backgrounds as $background): ?>
            <li>
             <div class="background_section">
                <img src="<?= $background['home_image'] ?>" />
                  <?php if($background['value_prop']): ?>
                         <h2 class="flex-caption"><?= $background['value_prop'] ?></h2>
                  <?php endif; ?>
              </div>
            </li>
        <?php endforeach; ?>
      </ul><!--.slides--> 
    </div><!--.flexslider.home_slider--> 
      
    <div class="clear"></div>  
    
    <?php endif;?>
    
    <div id="header_wrap">  	  
      <header id="branding" role="banner"> 
            <hgroup id="headergroup">
                <h1 id="site_title">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="home_link">
                        <img src="<?php bloginfo('template_url'); ?>/images/logo2.png" alt="" />
                        <span><?php bloginfo( 'name' ); ?></span>
                    </a>
                </h1>
                <h4 class="subheader"><?php bloginfo('description'); ?></h4>
            </hgroup>            
            <nav id="access" role="navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
            </nav><!-- #access -->    
    </header><!-- #branding -->		   	  
  </div><!-- #header_wrap -->
    
<?php endif;?>





<!--if a parallax page -->
<?php if($parallax_image): ?>
	<?php $para_image = wp_get_attachment_image_src($parallax_image, 'parallax-image'); ?>
  	<div class="parallax_img"><img src="<?php echo $para_image[0]; ?>" data-stellar-ratio="1.2" /></div>
<?php endif; ?>


        
        
        
    	
<div class="section_content section_<?php the_ID(); ?>">
	<a name="<?php the_ID(); ?>_bookmark" class="bookmark"></a>
	<div class="section_wrapper">
        
    	<header class="main_title">
        
            <h2><?php the_title(); ?></h2>
        
        </header>



         <?php if ($intro_text) { ?>
              <div class="intro_text"><?= $intro_text ?></div>
        <?php } ?>
        
        
        
        <?php if(get_the_content()) { ?>
			<div class="section_main_content">
				<?php the_content();?>
			</div>      
        <?php } ?>
 
        

        <?php if($sub_categories): ?>
        
        	<div class="category_blocks"> 
          
			<?php foreach($sub_categories as $sub_category): ?>
              
              <div class="category">
              
                <?php if($sub_category['category_icon']): ?>
                	<?php $cat_icon = wp_get_attachment_image_src($sub_category['category_icon'], 'icon_image'); ?>
                  <div class="category_icon"><img src="<?php echo $cat_icon[0]; ?>" /></div>
                <?php endif; ?>
                 
                <?php if($sub_category['category_title']): ?>
                    <h3><?= $sub_category['category_title'] ?></h3>
                <?php endif; ?>
                
                <?php if($sub_category['category_text']): ?>
                    <?= $sub_category['category_text'] ?>
                <?php endif; ?>
              
              </div><!--.category-->  
                    
            <?php endforeach; ?>
            
            <div class="clear"></div>
            
            </div><!--.category_blocks--> 
            
        <?php endif;?>
        
        
        
        
        <!-- gallery page -->
        <?php if($gallery_categories): ?>
        
            <div class="gallery_all">
             
			<?php foreach($gallery_categories as $gallery_category): ?>
                
					<?php if($gallery_category['category_title']): ?>
                        <div class="gallery_btn <?= $gallery_category['category_title'] ?>_btn"><a href="#"><?= $gallery_category['category_title'] ?></a></div>
                    <?php endif; ?>
                               
                
                  <div class="flexslider gallery <?= $gallery_category['category_title'] ?>_slider">
                    <ul class="slides">
                    
						<?php $images = $gallery_category['images']; ?>
                        <?php if($images): ?>
                                <?php foreach($images as $image): ?>
                                    <li class="<?= $gallery_category['category_title'] ?>">
                                        <?php $gallery_img = wp_get_attachment_image_src($image['gallery_image'], 'gallery-image-large'); ?>
                                        <img src="<?php echo $gallery_img[0]; ?>" />
                                        <?php if($image['image_caption']): ?>
                                            <div class="flex-caption"><?= $image['image_caption'] ?></div>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                         <?php endif; ?> 
    
                      </ul><!--.slides--> 
                  </div><!--.flexslider.trips_slider-->  
 
                
            <?php endforeach; ?>
            
            
            	<div class="clear"></div>
            </div><!--.gallery_all--> 
        
        <?php endif;?>
        
        
        
        
        
        
        
        <!-- blog page -->
		<?php if ( get_the_id() == 17 ): ?>
         	<ul class="blog_feed">
             <?php foreach($recent_posts as $post): setup_postdata($post); ?>
                
                    <li class="clearfix">
                    	<div class="post_meta">
							<?php the_time('F j, Y'); ?>
                            <div class="author_name"><?php the_author(); ?></div>
                        </div>
                        <div class="post_content">
                        	<h4><a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>"><?php the_title(); ?></a></h4>
                        	<span class="news_excerpt"><?php the_excerpt(); ?></span>
                        </div>
                    </li>
                 
              <?php endforeach; ?>
              </ul>
              
              <div class="more_blog"><a href="<?php echo esc_url( home_url( '/' ) ); ?>blog/">Read more</a></div>
        
        <?php endif; ?> 
        
        
        
   </div><!--.section_wrapper-->
</div><!--.section_content-->





        
        
        
    </section>

    <?php endwhile; endif; 
    	/* loop ends */
	} ?>   
        
        
    </div><!-- #main -->
   


<?php get_footer(); ?>